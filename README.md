<div style="display: flex; align-items: center;">
    <img src="https://graphql.org/img/logo.svg" height="50px" width="50px">
    <span>
        <strong>&nbsp; Crash Course | An Introduction to&nbsp;<span style="font-size: 22px"><a href="https://graphql.org">GraphQL</a></span></strong>
    </span>
</div>

---
**tldr; GraphQL is the new kid on the block for building REST apis that come with significant improvements to development.**

**[Skip to demo](#i-demo)** if you just want to see the code, but I highly encourage having a basic understanding of what GraphQL is and why it exists today.

[GraphQL](https://en.wikipedia.org/wiki/GraphQL) "is an open-source data query and manipulation language for APIs, and a runtime for fulfilling queries with existing data. GraphQL was developed internally by Facebook in 2012 before being publicly released in 2015."  The GraphQL [specification](https://graphql.github.io/graphql-spec/June2018/) is not tied to any implementation, it is just a spec.  Thus as long as an implementation obeys the spec, GraphQL can be run (i.e. java, node, ruby).  Additionally, it continues improving upon the existing REST conventions and earned its reputation by the open source community as [the 'better' REST](https://www.howtographql.com/basics/1-graphql-is-the-better-rest/).  Why is that?

Arguably, one of the most important [differences between APIs built using traditional REST and GraphQL](https://blog.apollographql.com/graphql-vs-rest-5d425123e34b) is knowing up front what data the client will receive.  Traditional REST conventions expose endpoints that provide a fixed data structure, meaning no matter what request parameters or how many times the client calls the endpoint, the data’s structure will always be the same.  However, with GraphQL, the client gets exactly what structure it needs because the data needed is declared up front via declarative data fetching allowing the flexibility for the client to pick and choose what data it can request.  This is possible because of one foundational idea GraphQL is founded upon—a schema backed API, using GraphQL schema definition language (SDL).

Having a schema changes the ball game because clients no longer have to guess what specific API they need to call and assume it will return the 'right' data the client needs.  Think about it, it’s analogous to the concept of interfaces, i.e. a binding contract, that any implementation of said interface must obey.  This means that any and all teams using the GraphQL API is independent of the implementation it isn’t responsible for; the frontend engineer knows what data they will receive and only needs to care they will get it, not how, and the backend engineer knows what data they will need to serve and not necessarily care who will use it.  As a result, the amount of overhead going back and forth between the front-end and back-end is minimized, giving back time to developers.  This concern that has existed since the rise of REST apis is called the over-fetching/under-fetching problem.  However, this has been solved with GraphQL.  Read more about it [here](https://www.howtographql.com/basics/1-graphql-is-the-better-rest/).

GraphQL’s SDL features out of the box a type system that ties heavily with the schemas defined.  The type system enforces strict validation on the schema and also lends itself to one of the most productive tooling and technique for any consumer of the GraphQL API; GraphiQL and introspection.  In layman’s terms, GraphiQL is a user interface provided by the GraphQL team and introspection is self-reflection.  For GraphQL, it means developers now have the means to examine the schema, even if the backend is inaccessible to them.  Learn more about introspection [here](https://graphqlmastery.com/blog/graphql-introspection-and-introspection-queries).

On a smaller note, GraphQL is also agnostic to transport protocol and data-source, meaning it works on other network protocols such as TCP and websockets, not just HTTP and can be tied to multiple data-sources of different types.  GraphQL can also be [greenfielded or integrated into a hybrid application](https://www.howtographql.com/basics/3-big-picture/).

<div align="center">
<h3 id="comparison-traditional-vs-graphql"><strong>Comparison: Traditional vs GraphQL</strong></h3>

|Traditional                       |GraphQL|
|----------------------------------|---------|
|Schema-less                       |Schema-defined|
|Fixed data structure              |Declarative flexible data fetching|
|Non-typed                         |Type system|
|Over/under-fetching problem       |Over/under-fetching solved|
|n endpoints                       |1 single endpoint|
|More effort required on dev team  |Less effort on dev team|
||GraphiQL|
||Transport-layer agnostic|
||Data-source agnostic|
</div>
<br/>
<p>Enough with the concepts, let's step through a practical with a simple POC about puppies. Code repository can be found <a href="https://gitlab.com/hl6510/puppies">here</a>.</p>

---

**DISCLAIMER: I assume the reader is already familiar with how web applications work and though all the code for a web application is here, this demo does not go into detail on how to setup a general web application.  The main focus is on GraphQL.**

<h2 id="overview"><strong>Overview</strong></h2>
<ol style="list-style-type: upper-roman">
    <li><a href="#i-demo">Demo</a></li>
    <li><a href="#ii-queries">Queries</a></li>
    <li><a href="#iii-query-fragments">Query Fragments</a></li>
	<li><a href="#iv-the-type-system-introspection">The Type System & Introspection</a></li>
    <li><a href="#v-mutations">Mutations</a></li>
    <li><a href="#vi-resolvers">Resolvers</a></li>
    <li><a href="#vii-technologies-frameworks">Technologies & Frameworks</a></li>
</ol>

Before we get started, go ahead and clone the repository and run the command `yarn && yarn build && yarn start` to run the application.

```bash
git clone https://gitlab.com/hl6510/puppies && cd puppies
yarn && yarn build && yarn start

// visit localhost:3000 in browser
```
<h2 id="i-demo">I. Demo</h2>
<p>In a nutshell, this application is about rescuing/abandoning (yikes) puppies. Every puppy image with the blue outline has been rescued(persisted) to the server but a puppy is abandoned if they have already been rescued but are then clicked again.  The FAB at the bottom right loads more images of puppies that want to be saved.  How many will you save?</p>

Upon successfully loading the demo application you should see the below image.

<div align="center">
    <img src="./images/demo_landing.png" width="60%" height="60%" style="max-width: 500px;">
    <div style="">
        <p style="text-align: center">
            <em>The sample demo</em>
        </p>
    </div>
</div>

Great, so what's going on?  Here's the breakdown.
<ol>
    <li><a href="#server-side">Server-side</a></li>
    <li><a href="#client-side">Client-side</a></li>
</ol>

<h3 id="server-side"><strong>Server-side</strong></h3>
<p>There are two main steps + prework to getting a GraphQL server up and running.</p>
<ol style="list-style-type: lower-roman">
    <li>
        <p>Let's start with the pre-work.</p>
        <ol style="list-style-type: lower-alpha">
            <li>
                <p><strong>Define business domain level types</strong> - This tells GraphQL about the custom types we want defined in our schema.  This is usually modeled after your business domain.</p>
                <div align="center"><img src="./images/graphql-custom-types.png" width="60%" height="60%" style="max-width: 500px;"></div>
            </li>
            <li>
                <p><strong>Create the models for those types</strong> - These are responsible for acting as the containers for your types.  They'll hold the data we define.</p>
                <div align="center"><img src="./images/graphql-models.png" width="60%" height="60%" style="max-width: 500px;"></div>
            </li>
        </ol>
    </li>
    <li>
        <p><strong>Define the schema</strong> - With the pre-work completed, we can now construct the schema.</p>
        <div align="center"><img src="./images/graphql-schema.png" width="60%" height="60%" style="max-width: 500px;"></div>
    </li>
    <li>
        <p><strong>Implement resolvers</strong> - Not necessarily needed since GraphQL provides a default resolver, but is highly recommended to define your own if you have custom logic or if the default doesn't fit your needs.</p>
        <div align="center"><img src="./images/graphql-resolvers.png" width="60%" height="60%" style="max-width: 500px;"></div>
    </li>
</ol>

<p>Altogether the server-side graphql setup should look something like <a href="./images/graphql-full-setup.png">this</a>.</p>

<h3 id="client-side"><strong>Client-side</strong></h3>
<ul style="list-style-type: lower-roman">
<li>
    <p><strong>Declare queries & mutations</strong> - These are the same queries you can test with in GraphiQL.  Copy and paste.</p>
    <div align="center"><img src="./images/client-all-queries.png" width="60%" height="60%" style="max-width: 500px;"></div>
</li>
<li>
    <p><strong>Do the request!</strong> - Bundle your query into a <code>fetch()</code> call or some form of AJAX request and handle the response.</p>
    <div align="center"><img src="./images/client-fetch.png" width="60%" height="60%" style="max-width: 500px;"></div>
</li>
</ul>

<p>Now that we've seen the code, let's dive a bit more into what's happening with GraphQL.</p>

<h3 id="ii-queries">II. Queries</h3>

<p>In a standard CRUD application, using the <code>query</code> keyword <strong>R</strong>eads from the server and allows us to retrieve the data we’ve asked for.  Though it is syntactic sugar, there are two ways of writing queries:</p>
<ul>
    <li>
        <p>Anonymous queries and named queries</p>        

    {       
        hello
    }

    // same as above, but below is the recommended convention
    query {
        hello
    }

</li>    
    <li>
        <p>Can pass parameters</p>

    // specify a parameter
    query getLeo{
        puppy(id: "leo") {
            name
            url
        }
    }

    // specify a default value for parameters
    query getPuppyByName($puppyName: ID = "leo") {
        puppy(id : $puppyName) {
            name
            url
        }
    }
        
</li>
</ul>

<h3 id="iii-query-fragments">III. Query Fragments</h3>
<p>Fragments are reusable code to build GraphQL queries/mutations.</p>

    query getLeo {
        puppy(id: "leo") {
            ...puppyMetadata
        }
    }

    query getPuppyByName($puppyName: ID = "leo") {
        puppy(id : $puppyName) {
            ...puppyMetadata
        }
    }

    // a reusable fragment
    fragment puppyMetadata on Puppy {
        name
        url
    }

<h3 id="iv-the-type-system-introspection">IV. The Type System & Introspection</h3>

<p>The GraphQL team provides out of the box a graphical interface, <a href="https://github.com/graphql/graphiql">GraphiQL</a>, that allows developers to query against the API defined by the schema.  Like <a href="https://www.getpostman.com">Postman</a>, a popular API development tool, GraphiQL is a convenient tool that you can run requests/queries with on the fly—and you don’t need an separate installation for it.</p>

<div align="center">
    <img src="./images/graphiql_screen.png" width="60%" height="60%">
    <div style="">
        <p style="text-align: center">
            <em>The GUI for GraphiQL</em>
        </p>
    </div>
</div>

<p>Because GraphQL is typed, we can ask immediately ask the schema what is available for the client to use.  Any types prefixed with a double underscore are specific to GraphQL and are internally used.  These types are called dunders i.e. <code>__schema</code>, <code>__types</code>.</p>

    // returns list of all types and their name, description as defined by the schema, including internal GraphQL types
    query {
        __schema {
            types {
                name
                description
            }
        }
    }

    // returns name, description, and fields with sub-fields name, description for the type with name "Puppy"
    query {
        __type (name: "Puppy") {
            name
            description
            ...typeMetadata
        }
    }

    query {
        __type (name: "Mutation") {
            name
            description
            ...typeMetadata
        }
    }

    fragment typeMetadata on __Type {
        fields {
            name
            description
        }
    }

<h3 id="v-mutations">V. Mutations</h3>

<p>The GraphQL way of <strong>C</strong>reating, <strong>U</strong>pdating and <strong>D</strong>eleting data.</p>

    mutation {
        // returns the newly created Puppy instance
        createPuppy(url: "some url of type String") {
            name
            id
            birthday
            url
        }
    }

    mutation {
        // returns an instance of Message
        deletePuppy(id: "some id of type ID") {
            id
            message
        }
    }

<h3 id="vi-resolvers">VI. Resolvers</h3>
<p>When a client sends a request with a query, the server cascades down a list of resolvers for that query and every resolver is executed to return the requested data.  A schema requires resolvers to return the data required by the client.  Though resolvers are required, GraphQL automatically provides a default resolver with a default implementation.  You’ll usually want to write your own resolvers for any custom logic.  But be careful, depending on the implementations you write, the resolvers are where you can have potential performance bottlenecks.</p>

<p>Here's a quick common use case, <a href="https://dev.to/jackmarchant/offset-and-cursor-pagination-explained-b89">pagination</a>.  Typically, there are 2 general implementations.</p>
<ul>
    <li>Limit offset (easier to implement, not as performant with increasing complexity)</li>
    <li>Cursor based (not so straightforward but better performance)</li>
</ul>

<p>Depending on the needs of the client, you'd want to keep in mind the implementations chosen for these resolvers.</p>
<div align="center">
<h3 id="vii-technologies-frameworks">VII. Technologies & Frameworks</h3>

|   Technologies    |   Frameworks                                        |
|-----------------------------------------------------------|-------------|
|React: React-Apollo, Relay, <strong>urql (newest!)</strong>| Apollo|
|Angular: apollo-angular                                    | Relay|
|Swift/iOS: Apollo iOS                                      | urql|
|Java/Android: Apollo Android||
|JavaScript: GraphQL.js reference implementation, graphql-tools||
|Ruby: graphql-ruby||
|Golang: graphql-go||
|Scala: Sangria||
|Python: Graphene||
|<strong>GraphQL Faker!</strong> - stub out your data!||
</div>
</br>
<p>In conclusion, GraphQL is definitely worth experimenting with.  Its an improvement to the web development landscape and the advantages it offers are invaluable to the development experience and the pace at which teams can deliver.</p>

<h3 id="faq"><a href="https://www.howtographql.com/advanced/5-common-questions/">FAQ</a></h3>

---

<h2 id="references">References</h2>

    “Common GraphQL Questions.” Common GraphQL Questions, https://www.howtographql.com/advanced/5-common-questions/.

    “GraphQL.” Wikipedia, Wikimedia Foundation, 14 Nov. 2019, https://en.wikipedia.org/wiki/GraphQL.

    “GraphQL vs REST - A Comparison.” GraphQL vs REST - A Comparison, https://www.howtographql.com/basics/1-graphql-is-the-better-rest/.

    GraphQL, https://graphql.github.io/graphql-spec/June2018/.

    “GraphQL Server Basics: GraphQL Schemas, TypeDefs & Resolvers Explained.” Prisma, https://www.prisma.io/blog/graphql-server-basics-the-schema-ac5e2950214e.

    Marchant, Jack. “Offset and Cursor Pagination Explained.” The DEV Community, https://dev.to/jackmarchant/offset-and-cursor-pagination-explained-b89.

    Mráz, David. “GraphQL Introspection and Introspection Queries.” GraphQL Mastery, Atheros Intelligence Ltd., 23 Apr. 2019, https://graphqlmastery.com/blog/graphql-introspection-and-introspection-queries.

    Stubailo, Sashko. “GraphQL vs. REST.” Medium, Apollo GraphQL, 6 Apr. 2018, https://blog.apollographql.com/graphql-vs-rest-5d425123e34b.