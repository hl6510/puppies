const gulp = require('gulp'),
      babel = require('gulp-babel');

const CONFIG = {
    presets: ['@babel/env']
  };

  gulp.task('serverJS', () => {
    return gulp.src('src/*.js')
        .pipe(babel(CONFIG))
        .pipe(gulp.dest('build'));
  });

  gulp.task('routes', () => {
    return gulp.src('src/routes/*.js')
        .pipe(babel(CONFIG))
        .pipe(gulp.dest('build/routes'));
  });

  gulp.task('css', () => {
    return gulp.src('src/public/css/*.css')
        .pipe(gulp.dest('build/public/css'));
  });

  gulp.task('img', () => {
    return gulp.src('src/public/images/*')
    .pipe(gulp.dest('build/public/images'))
  });

  gulp.task('clientJS', () => {
    return gulp.src('src/public/js/*.js')
        .pipe(babel(CONFIG))
        .pipe(gulp.dest('build/public/js'));
  })

  gulp.task('watch', () => {
    gulp.watch(
      [
        'src/*.js',
        'src/routes/*.js',
        'src/public/css/*.css',
        'src/public/js/*.js',
        'src/public/images/*'
      ],
      gulp.parallel('serverJS', 'routes', 'css', 'clientJS', 'img')
    );
  });

  gulp.task('default', gulp.series('serverJS', 'routes', 'css', 'clientJS', 'img'));
  gulp.task('watch-default', gulp.series('serverJS', 'routes', 'css', 'clientJS', 'img', gulp.parallel('watch')));
