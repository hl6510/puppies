// https://dog.ceo/dog-api/
const PUPPIES_ENDPOINT = `https://dog.ceo/api/breeds/image/random`;
const SERVER_ENDPOINT = window.location.hostname === 'localhost' ? `http://` + window.location.hostname + `:3000/graphiql` : `http://`+ window.location.hostname + `/graphiql`;

const MSNRY = new Masonry(document.getElementById('grid-puppies'), {
  itemSelector: '.grid-puppy',
  transitionDuration: '0.3s',
  percentPosition: true
});

const QUERIES = {
  getPuppy: `query {
    puppies {
      page {
        id
        url
      }
    }
  }`,
  savePuppy: `mutation createPuppy($url: String!) {
    createPuppy(url: $url) {
      id
      birthday
      name
      url
    }
  }`,
  deletePuppy: `mutation deletePuppy($id: ID!) {
    deletePuppy(id: $id) {
      message
    }
  }`
};

let count = 0;
const OPTS = {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json',
    'Accept': 'application/json'
  }
};

const savePuppy = event => {
  const isSaved = event.currentTarget.classList.contains('saved');

  const query = isSaved ? QUERIES.deletePuppy : QUERIES.savePuppy;
  const variables = isSaved ? {
    id: event.currentTarget.id,
  } : {
    url: event.path ? event.path[0].currentSrc : event.target.currentSrc
  };

  if (isSaved) {
    console.log('You abandoned a puppy.  Puppies saved: ' + --count);
    event.currentTarget.classList.toggle('saved', false);
  } else {
    console.log('You saved a puppy.  Puppies saved: ' + ++count);
    event.currentTarget.classList.toggle('saved', true);
  }

  const reqInfo = {
    method: OPTS.method,
    headers: OPTS.headers,
    body: JSON.stringify({
      query,
      variables
    })
  }

  fetch(SERVER_ENDPOINT, reqInfo)
    .then(res => res.json())
    .then(data => console.log(`Data returned`, data))
    .catch(err => console.log(`Encountered err`, err));
}

const showPuppy = data => {
  const puppyBox = document.createElement('div');

  puppyBox.addEventListener('mouseover', e => { e.currentTarget.classList.toggle('puppy-hover', true)} );
  puppyBox.addEventListener('mouseout', e => { e.currentTarget.classList.toggle('puppy-hover', false)} );

  const puppyImage = document.createElement('img');
    puppyImage.setAttribute('src', data.message || data.url);
    puppyImage.setAttribute('class', 'grid-puppy');
    puppyImage.setAttribute('id', data.id);
    puppyImage.addEventListener('click', savePuppy);

  puppyBox.appendChild(puppyImage);

  if (data.id) {
    puppyImage.setAttribute('class', 'grid-puppy saved');
  }

  document.getElementById('grid-puppies').append(puppyBox);
  MSNRY.appended(puppyBox);
  imagesLoaded('#grid-puppies').on('progress', () => MSNRY.layout());
};

const showPuppies = ({ data }) => {
  for (let puppy of data.puppies.page) {
    showPuppy(puppy);
  }
};

const getNextPuppy = () => {
  fetch(PUPPIES_ENDPOINT)
    .then(res => res.json())
    .then(data => showPuppy(data));
};

window.onload = () => {
  const reqInfo = {
    method: OPTS.method,
    headers: OPTS.headers,
    body: JSON.stringify({
      query: QUERIES.getPuppy
    })
  };

  fetch(SERVER_ENDPOINT, reqInfo)
    .then(res => res.json())
    .then(res => showPuppies(res))
    .catch(err => console.log(`Unable to load puppies`, err));

  fetch(PUPPIES_ENDPOINT)
    .then(res => res.json())
    .then(data => showPuppy(data))
    .catch(err => console.log(`Unable to get puppy picture`, err));
};
