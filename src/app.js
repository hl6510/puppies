import dotenv from 'dotenv';
dotenv.load();

import graphqlHTTP from 'express-graphql';
import express from 'express';
import path from 'path';
import routes from './routes/index';
import options from './routes/graphql';

let app = express();

app.set('views', path.join(__dirname, '../views'));
app.set('view engine', 'pug');

app.use('/graphiql', graphqlHTTP(options));
app.use('/graphql', graphqlHTTP({ ...options, graphiql: false }));

app.use('/', routes);
app.use(express.static(path.join(__dirname, 'public')));

/* Uncomment when favicon has been added */
// app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

module.exports = app;
