import data from './data';

// see OP https://stackoverflow.com/questions/9035627/elegant-method-to-generate-array-of-random-dates-within-two-dates
export function randomDate(start, end) {
    return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
}

export function randomName() {
    const nameIdx = Math.round(Math.random() * 100);
    return data.names[nameIdx];
}
