
import {
  GraphQLBoolean,
  GraphQLID,
  GraphQLList,
  GraphQLInt,
  GraphQLNonNull,
  GraphQLSchema,
  GraphQLObjectType,
  GraphQLString,
} from 'graphql';

import db from './db';
import { randomDate, randomName } from '../util';

const graphiql = true;

// Construct a schema, using GraphQL schema language

const PuppyType = new GraphQLObjectType({
  name: 'Puppy',
  description: 'The wrapper for puppy',
  fields: {
    id: {
      type: GraphQLID,
      description: 'The id of the puppy image'
    },
    birthday: {
      type: GraphQLString,
      description: 'The birthday of the puppy'
    },
    name: {
      type: GraphQLString,
      description: 'The name of the puppy'
    },
    url: {
      type: GraphQLString,
      description: 'The url to puppy image'
    }
  }
});

const PuppiesType = new GraphQLObjectType({
  name: 'Puppies',
  description: 'Returns a page of puppies',
  fields: {
    page: {
      type: new GraphQLNonNull(GraphQLList(PuppyType)),
      description: 'A page of puppies'
    },
    pageNum: {
      type: GraphQLInt
    },
    hasNextPage: {
      type: GraphQLBoolean
    }
  }
});

const MessageType = new GraphQLObjectType({
  name: 'Message',
  description: 'The wrapper for message',
  fields: {
    id: {
      type: GraphQLID,
      description: 'The id of the message'
    },
    message: {
      type: GraphQLString,
      description: 'The message text'
    }
  }
});

export const schema = new GraphQLSchema({
  query: new GraphQLObjectType({
    name: 'RootQueryType',
    fields: {
      hello: {
        type: GraphQLString,
        resolve() {
          return 'world';
        },
      },
      puppy: {
        type: PuppyType,
        args: {
          id: {
            type: GraphQLID
          }
        },
        resolve(_, { id }) {
          if (!id) {
            return new Puppy(db[`leo`]);
          };
          if (db[id]) {
            return new Puppy(db[id]);
          }
          throw new Error('No puppy exists with id: ' + id);
        }
      },
      puppies: {
        type: PuppiesType,
        args: {
          pageNum: {
            type: GraphQLInt
          },
          pageSize: {
            type: GraphQLInt
          }
        },
        resolve(_, { pageNum, pageSize }) {
          const ids = Object.keys(db);
          let numPages, hasNextPage = false, puppies = [];

          if (pageNum > 0 && pageSize > 0) {
            numPages = Math.ceil(ids.length / pageSize);

            // validate pageNum exists in numPages
            const pageToReturn = Math.min(numPages, pageNum);
            for (let itemIdx = ((pageToReturn - 1) * pageSize); itemIdx < (pageToReturn * pageSize); itemIdx++) {
              const existsPuppy = db[ids[itemIdx]] !== null && db[ids[itemIdx]] !== undefined;
              if (existsPuppy) {
                puppies.push(db[ids[itemIdx]]);
              } else {
                break;
              }
            }
            hasNextPage = pageToReturn < numPages;
          } else {
            for (let id of ids) {
              puppies.push(db[id]);
            }
          }
          return new Puppies(puppies, pageNum, hasNextPage);
        }
      }
    },
  }),
  mutation: new GraphQLObjectType({
    name: 'Mutation',
    fields: {
      createPuppy: {
        type: PuppyType,
        args: {
          url: { type: GraphQLString }
        },
        resolve(_, { url }) {
          const id = require('crypto').randomBytes(10).toString('hex');
          db[id] = {
            id,
            birthday: randomDate(new Date(2012, 0, 1), new Date()).toDateString(),
            name: randomName(),
            url
          };
          return new Puppy(db[id]);
        }
      },
      deletePuppy: {
        type: MessageType,
        args: {
          id: { type: GraphQLID }
        },
        resolve(_, { id }) {
          const puppyExists = db[id];
          if (puppyExists) {
            delete db[id];
            return new Message({ message: "Puppy " + id + " deleted" });
          }
          throw new Error('Puppy does not exist');
        }
      }
    }
  })
});

// models
class Puppy {
  constructor({ id, birthday, name, url }) {
    this.id = id;
    this.birthday = birthday;
    this.name = name;
    this.url = url;
  }
}

class Puppies {
  constructor(page, pageNum, hasNextPage) {
    this.page = page;
    this.pageNum = pageNum;
    this.hasNextPage = hasNextPage;
  }
}

class Message {
  constructor({ message }) {
    this.message = message;
  }
}

// root query
export const rootValue = {
  puppy: puppy => puppy,
  puppies: puppies => puppies,
  createPuppy: puppy => puppy,
  deletePuppy: message => message
};

export default {
  schema,
  rootValue,
  graphiql
};
