const db = {
    "6ac374b9143497d3b680" : { id: `6ac374b9143497d3b680`, birthday: '25 August 2018', name: `Leo`, url: `https://images.dog.ceo/breeds/whippet/n02091134_2349.jpg` },
    "e0cbbad764821992ebd7" : { id: `e0cbbad764821992ebd7`, birthday: '25 August 2018', name: `Leo`, url: `https://images.dog.ceo/breeds/cotondetulear/100_2397.jpg` },
    "300d323bcae0ea59517b" : { id: `300d323bcae0ea59517b`, birthday: '25 August 2018', name: `Leo`, url: `https://images.dog.ceo/breeds/terrier-russell/jack2.jpg` },
    "899449b8ffb0f542ec7a" : { id: `899449b8ffb0f542ec7a`, birthday: '25 August 2018', name: `Leo`, url: `https://images.dog.ceo/breeds/terrier-american/n02093428_1278.jpg` },
    "4e0627613813683b9a63" : { id: `4e0627613813683b9a63`, birthday: '25 August 2018', name: `Leo`, url: `https://images.dog.ceo/breeds/doberman/n02107142_12749.jpg` },
    "cb4e45a8c5a35f2a11f6" : { id: `cb4e45a8c5a35f2a11f6`, birthday: '25 August 2018', name: `Leo`, url: `https://images.dog.ceo/breeds/malinois/n02105162_5330.jpg` },
    "eef41c4653ecda1f8b37" : { id: `eef41c4653ecda1f8b37`, birthday: '25 August 2018', name: `Leo`, url: `https://images.dog.ceo/breeds/labrador/n02099712_6823.jpg` },
    "2762b26036bfee93e869" : { id: `2762b26036bfee93e869`, birthday: '25 August 2018', name: `Leo`, url: `https://images.dog.ceo/breeds/pyrenees/n02111500_1943.jpg` },
    "100b7fa04eabfc5cc899" : { id: `100b7fa04eabfc5cc899`, birthday: '25 August 2018', name: `Leo`, url: `https://images.dog.ceo/breeds/pointer-germanlonghair/hans3.jpg` },
    "leo": { id: `leo`, birthday: '25 August 2018', name: `The Real Leo`, url: `images/leo.png` }
};

export default db;